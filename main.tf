 provider "aws" {
     region = "us-east-1a"
 }

 variable "subnet_cidr_block" {
     description = "subnet cidr block" 
 }

 variable "vpc_cidr_block" {
     description = "vpc cidr block"
 }
 variable "vpc_default_cidr_block" {
   description = "vpc default_cidr block"
 }
 resource "aws_vpc" "development-vpc" {
   cidr_block = var.subnet_cidr_block
   tags = {
       Name: "development"
   }
 }

 resource "aws_subnet" "dev-subnet-1" {
   vpc_id = aws_vpc.development-vpc.id
   cidr_block = var.vpc_cidr_block
   availability_zone = "us-east-1a"
   tags = {
       Name: "Subnet-1-dev"
   }
 }

 
 data "aws_vpc" "exisiting_vpc" {
    default = true
 }

 resource "aws_subnet" "dev-subnet-2" {
   vpc_id = data.aws_vpc.exisiting_vpc.id
   cidr_block = var.vpc_default_cidr_block
   availability_zone = "us-east-1a"
   tags = {
       Name: "Subnet-2-default"
   }
 }

